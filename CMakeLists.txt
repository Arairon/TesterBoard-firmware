cmake_minimum_required(VERSION 3.20)

project("TesterBoard" C CXX ASM)

include(cmake/st-project.cmake)

add_executable(${PROJECT_NAME})
add_st_target_properties(${PROJECT_NAME})

include_directories(Inc)

target_sources(
    ${PROJECT_NAME} PRIVATE
    "Core\\Inc\\libs\\bmp280\\sg_bmp280.c"
    "Core\\Inc\\libs\\lis3mdl\\SG_LIS3MDL.c"
    "Core\\Inc\\libs\\lsm6ds3\\sg_lsm6ds3.c"
    "Core\\Inc\\libs\\sg_stm32g0_i2c\\sg_stm32g0_i2c.c"
)
#target_include_directories(
#    ${PROJECT_NAME} PRIVATE
#    "${PROJECT_SOURCE_DIR}/Inc\\libs\\*>"
#)
#set(SOURCES file.cpp file2.cpp ${YOUR_DIRECTORY}/file1.h ${YOUR_DIRECTORY}/file2.h)
#add_executable(test ${SOURCES})
# SG lib for BMP280

**[ EN ]** This is a library for interacting with BMP280 with built-in pressure and temperature compensation.

**[ RU ]** Это библиотека для взаимодействия с BMP280 с встроенной компенсацией для давления и температуры.

## Functions

### SG_BMP280_init(void)

**[ EN ]** Loads settings into BMP280 by SG_BMP280_SLAVE_ADDRESS *(can be configured in sg_config.h)*.
Reads factory calibration data for further t/p compensation.

**[ RU ]** Загружает настройки в BMP280 с i2c адресом SG_BMP280_SLAVE_ADDRESS *(Можно настроить в sg_config.h)*.
Затем считывает заводские данные калибровки для последующей компенсации значений.

### SG_BMP280_read_t_and_p(uint32_t\* presssure int16_t\* temperature)

**[ EN ]** Accepts an uint32 pointer for pressure and an int16 pointer for temperature.
Reads data from BMP280, runs compensation and writes it to pointers.

**[ RU ]** Принимает указатель на uint32 для давления и указатель на int16 для температуры.
Считывает данные с BMP280, компенсирует их по заводским значениям и записывает в переменные по указателям.

----

Usage example:

```c
SG_I2C1_Init(); // this lib depends on SG's i2c lib

SG_BMP280_init();

int16_t temperature;
uint32_t pressure;

SG_BMP280_read_t_and_p(&pressure, &temperature);
```

----

Config example:

```h
#ifndef __SG_CONFIG_H
#define __SG_CONFIG_H

#define SG_BMP280_SLAVE_ADDRESS 0b11101101

#endif
```

#include "SG_BMP280.h"

uint16_t dig_T1;
int16_t dig_T2;
int16_t dig_T3;
int32_t t_fine;

uint16_t dig_P1;
int16_t dig_P2;
int16_t dig_P3;
int16_t dig_P4;
int16_t dig_P5;
int16_t dig_P6;
int16_t dig_P7;
int16_t dig_P8;
int16_t dig_P9;

int32_t t_fine;

void SG_BMP280_calibrate(void);
void SG_BMP280_read_registers(uint8_t* data);

void SG_BMP280_init(){
	//load settings into BMP280 register
	SG_I2C_Write_Register(SG_BMP280_SLAVE_ADDRESS, 0xF4, 0b00100111); //001-pressure oversampling, 001-temperature oversampling, 11-normal mode
	//read factory calibration data
	SG_BMP280_calibrate();
}

int32_t SG_BMP280_compensate_T_int32(int32_t adc_T)
{
	int32_t var1, var2, T;
	var1 = ((((adc_T>>3) - ((int32_t)dig_T1<<1))) * ((int32_t)dig_T2)) >> 11;
	var2 = (((((adc_T>>4) - ((int32_t)dig_T1)) * ((adc_T>>4) - ((int32_t)dig_T1))) >> 12) *
	((int32_t)dig_T3)) >> 14;
	t_fine = var1 + var2;
	T = (t_fine * 5 + 128) >> 8;
	return T;
}
uint32_t SG_BMP280_compensate_P_int32(int32_t adc_P)
{
	int32_t var1, var2;
	uint32_t p;
	var1 = (((int32_t)t_fine)>>1) - (int32_t)64000;
	var2 = (((var1>>2) * (var1>>2)) >> 11 ) * ((int32_t)dig_P6);
	var2 = var2 + ((var1*((int32_t)dig_P5))<<1);
	var2 = (var2>>2)+(((int32_t)dig_P4)<<16);
	var1 = (((dig_P3 * (((var1>>2) * (var1>>2)) >> 13 )) >> 3) + ((((int32_t)dig_P2) * var1)>>1))>>18;
	var1 =((((32768+var1))*((int32_t)dig_P1))>>15);
	if (var1 == 0){
		return 0; // avoid exception caused by division by zero
	}
	p = (((uint32_t)(((int32_t)1048576)-adc_P)-(var2>>12)))*3125;
	if (p < 0x80000000){
		p = (p << 1) / ((uint32_t)var1);
	}else{
		p = (p / (uint32_t)var1) * 2;
	}
	var1 = (((int32_t)dig_P9) * ((int32_t)(((p>>3) * (p>>3))>>13)))>>12;
	var2 = (((int32_t)(p>>2)) * ((int32_t)dig_P8))>>13;
	p = (uint32_t)((int32_t)p + ((var1 + var2 + dig_P7) >> 4));
	return p;
}

inline void SG_BMP280_read_registers(uint8_t* data){
	//read all registers for p and t data
	SG_I2C_Read_Register_Multiple(SG_BMP280_SLAVE_ADDRESS, SG_BMP280_REG_DATA_START, data, 8);
}

void SG_BMP280_read_t_and_p(uint32_t* press_out, int16_t* temp_out){
	//define array for raw data
	uint8_t data[8];
	//read raw data from bme
	SG_BMP280_read_registers(data);

	//calculate temperature
	int32_t raw = ((uint32_t)data[3] << 12) | ((uint32_t)data[4] << 4) | ((uint32_t)data[5] >> 4);
	*temp_out = SG_BMP280_compensate_T_int32(raw);

	//calculate pressure
	raw = ((uint32_t)data[0] << 12) | ((uint32_t)data[1] << 4) | ((uint32_t)data[2] >> 4);
	*press_out = SG_BMP280_compensate_P_int32(raw);
}

void SG_BMP280_calibrate(){
	uint8_t calib_data[SG_BMP280_LENGTH_CALIBRATION];
	for (uint8_t i=0; i<SG_BMP280_LENGTH_CALIBRATION; i++){
		calib_data[i]=SG_I2C_Read_Register(SG_BMP280_SLAVE_ADDRESS, 0x88+i);
	}
	dig_T1 = (((uint16_t)calib_data[1]) << 8) | (calib_data[0]);
	dig_T2 = (((uint16_t)calib_data[3]) << 8) | (calib_data[2]);
	dig_T3 = (((uint16_t)calib_data[5]) << 8) | (calib_data[4]);
	dig_P1 = (((uint16_t)calib_data[7]) << 8) | (calib_data[6]);
	dig_P2 = (((uint16_t)calib_data[9]) << 8) | (calib_data[8]);
	dig_P3 = (((uint16_t)calib_data[11]) << 8) | (calib_data[10]);
	dig_P4 = (((uint16_t)calib_data[13]) << 8) | (calib_data[12]);
	dig_P5 = (((uint16_t)calib_data[15]) << 8) | (calib_data[14]);
	dig_P6 = (((uint16_t)calib_data[17]) << 8) | (calib_data[16]);
	dig_P7 = (((uint16_t)calib_data[19]) << 8) | (calib_data[18]);
	dig_P8 = (((uint16_t)calib_data[21]) << 8) | (calib_data[20]);
	dig_P9 = (((uint16_t)calib_data[23]) << 8) | (calib_data[22]);
}

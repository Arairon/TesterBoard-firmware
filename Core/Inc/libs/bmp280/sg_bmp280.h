#ifndef SG_BMP280_H
#define SG_BMP280_H

#include "main.h"

#if !defined(__STM32F4xx_HAL_H) && !defined(__STM32F1xx_HAL_H) && !defined(STM32G0xx_HAL_H)
#error ONLY G0, F1 or F4 MCU are supported!
#endif

#ifdef __STM32F4xx_HAL_H
#include "sg_stm32f4_i2c.h"
#endif
#ifdef __STM32F1xx_HAL_H
#include "sg_stm32f1_i2c.h"
#endif
#ifdef STM32G0xx_HAL_H
#include "libs/sg_stm32g0_i2c/sg_stm32g0_i2c.h"
#endif

#ifndef __SG_CONFIG_H
#warning It is recommended to create sg_config.h and include it before including this library
#endif



#ifndef SG_BMP280_SLAVE_ADDRESS
#define SG_BMP280_SLAVE_ADDRESS			0b11101101
#warning using default SG_BMP280_SLAVE_ADDRESS
#endif

#define SG_BMP280_LENGTH_CALIBRATION 	24

#define SG_BMP280_REG_DATA_START		0xF7

//Enabling sensor
void SG_BMP280_init(void);
//Reading data from sensor and converting it into pressure and temperature instead of raw data
//press_out - pointer (!) to a uint32_t variable for pressure
//temp_out - pointer (!) to a int16_t variable for temperature
void SG_BMP280_read_t_and_p(uint32_t* press_out, int16_t* temp_out);

#endif

# SG lib for LIS3MDL

**[ EN ]** This is a library for interacting with 3-axis magnetometer LIS3MDL.

**[ RU ]** Это библиотека для взаимодействия с 3-осевым магнитометром LIS3MDL.

## Functions

### SG_LIS3MDL_init(void)

**[ EN ]** Loads settings into lis3mdl by SG_LIS3MDL_SLAVE_ADDRESS *(can be configured in sg_config.h)*.

**[ RU ]** Загружает настройки в lis3mdl с i2c адресом SG_LIS3MDL_SLAVE_ADDRESS *(Можно настроить в sg_config.h)*.

### SG_LIS3MDL_ReadData(uint8_t *data)

**[ EN ]** Accepts a pointer to an array[6 or more] of uint8_t and writes data to the given array. See [datasheet (7.1-7.3)](https://www.st.com/resource/en/datasheet/lis3mdl.pdf) for formatting info.

**[ RU ]** Принимает указатель на массив uint8 длинною в 6 или больше и записывает в него данные. Формат указан в [тех. документации (7.1-7.3)](https://www.st.com/resource/en/datasheet/lis3mdl.pdf).

----

Usage example:

```c
SG_I2C1_Init(); // this lib depends on SG's i2c lib

SG_LIS3MDL_init();

uint8_t lis3_data[6];

SG_LIS3MDL_ReadData(lis3_data);

int16_t *ptr_m = (int16_t *)lis3_data;
// All following values are int16.
mag_x = ptr_m[0]; mag_y = ptr_m[1]; mag_z = ptr_m[2];
```

----
Config example:

See [datasheet (7.5 and 7.6)](https://www.st.com/resource/en/datasheet/lis3mdl.pdf) for more info.

```h
#ifndef __SG_CONFIG_H
#define __SG_CONFIG_H

//TEMP_EN - OFF, XY mode - High performance mode, Data rate - 10Hz, Self-test - disabled
#define LIS3MDL_REG1_VALUE                      0b01010000
//Full-scale - +-4 gauss, REBOOT - normal mode, SOFT_RST - default
#define LIS3MDL_REG2_VALUE                      0b00000000

#endif
```

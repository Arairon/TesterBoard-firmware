#include "SG_LIS3MDL.h"

///////////////////////CONFIG VALUES/////////////////////
#ifndef LIS3MDL_REG1_VALUE
#define LIS3MDL_REG1_VALUE					0b01010000
#warning LIS3MDL_REG1_VALUE defined by default: TEMP_EN - OFF, XY mode - High performance mode, Data rate - 10Hz, Self-test - disabled
#endif

#ifndef LIS3MDL_REG2_VALUE
#define LIS3MDL_REG2_VALUE					0b00000000
#warning LIS3MDL_REG2_VALUE defined bt default: Full-scale - +-4 gauss, REBOOT - normal mode, SOFT_RST - default
#endif

//Continuous-conversion mode
#define LIS3MDL_REG3_VALUE_CCM				0b00000000
//Power-down mode
#define LIS3MDL_REG3_VALUE_PDM				0b00000011

// Z mode - High performance mode
#define LIS3MDL_REG4_VALUE					0b00001000

//BDU - output registers not updated until MSb and LSb have been read
#define LIS3MDL_REG5_VALUE					0b01000000

void SG_LIS3MDL_init(){
	SG_I2C_Write_Register(SG_LIS3MDL_SLAVE_ADDRESS, LIS3MDL_CTRL_REG3, LIS3MDL_REG3_VALUE_PDM);
	HAL_Delay(1);
	SG_I2C_Write_Register(SG_LIS3MDL_SLAVE_ADDRESS, LIS3MDL_CTRL_REG1, LIS3MDL_REG1_VALUE);
	HAL_Delay(1);
	SG_I2C_Write_Register(SG_LIS3MDL_SLAVE_ADDRESS, LIS3MDL_CTRL_REG2, LIS3MDL_REG2_VALUE);
	HAL_Delay(1);
	SG_I2C_Write_Register(SG_LIS3MDL_SLAVE_ADDRESS, LIS3MDL_CTRL_REG4, LIS3MDL_REG4_VALUE);
	HAL_Delay(1);
	SG_I2C_Write_Register(SG_LIS3MDL_SLAVE_ADDRESS, LIS3MDL_CTRL_REG5, LIS3MDL_REG5_VALUE);
	HAL_Delay(1);
	SG_I2C_Write_Register(SG_LIS3MDL_SLAVE_ADDRESS, LIS3MDL_CTRL_REG3, LIS3MDL_REG3_VALUE_CCM);
	HAL_Delay(1);
}

void SG_LIS3MDL_ReadData(uint8_t *data){
	SG_I2C_Read_Register_Multiple(SG_LIS3MDL_SLAVE_ADDRESS, LIS3MDL_OUTX_L, data, 6 );
}

uint8_t SG_LIS3MDL_WhoAmI(){
	SG_I2C_Read_Register(SG_LIS3MDL_SLAVE_ADDRESS, LIS3MDL_WHO_AM_I_REG);
	return SG_I2C_Read_Register(SG_LIS3MDL_SLAVE_ADDRESS, LIS3MDL_WHO_AM_I_REG);
}

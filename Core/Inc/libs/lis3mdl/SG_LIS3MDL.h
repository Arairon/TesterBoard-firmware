#ifndef SG_LIS3MDL_H
#define SG_LIS3MDL_H

#include "main.h"
/*
#if !defined(__STM32F4xx_HAL_H) && !defined(__STM32F1xx_HAL_H) && !defined(__STM32G0xx_HAL_H)
#error ONLY G0, F1 or F4 MCU are supported!
#endif
*/

#ifndef __SG_CONFIG_H
#warning It is recommended to create sg_config.h and include it before including this library
#endif

//Init
void SG_LIS3MDL_init(void);

// reads magnetometer data from sensor
// data - pointer to an array, where to put the result
void SG_LIS3MDL_ReadData(uint8_t *data);
uint8_t SG_LIS3MDL_WhoAmI();

#ifndef	SG_LIS3MDL_SLAVE_ADDRESS
#define	SG_LIS3MDL_SLAVE_ADDRESS						0x3C
#endif

//////////////////////REGISTER ADDRESSES/////////////////////
#define	LIS3MDL_WHO_AM_I_REG				0X0F //default 0x3D
#define LIS3MDL_CTRL_REG1					0X20
#define	LIS3MDL_CTRL_REG2					0X21
#define	LIS3MDL_CTRL_REG3					0X22
#define	LIS3MDL_CTRL_REG4					0X23
#define LIS3MDL_CTRL_REG5					0X24
//
#define LIS3MDL_OUTX_L    					0X28
#define LIS3MDL_OUTX_H    					0X29
#define LIS3MDL_OUTY_L   					0X2A
#define LIS3MDL_OUTY_H    					0X2B
#define LIS3MDL_OUTZ_L    					0X2C
#define LIS3MDL_OUTZ_H   					0X2D
#define LIS3MDL_OUT_TEMP_L   				0X2E //нужно включить в CTRL_REG1
#define LIS3MDL_OUT_TEMP_H   				0X2F //нужно включить в CTRL_REG1
//
#define LIS3MDL_STATUS_REG					0x27
#define LIS3MDL_INT_SRC						0x31
#define LIS3MDL_INT_THS_L					0x32
#define LIS3MDL_INT_THS_H					0x33

#endif

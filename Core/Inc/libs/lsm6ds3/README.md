# SG lib for LSM6DS3

**[ EN ]** This is a library for interacting with 3-axis
accelerometer and gyroscope LSM6DS3.

**[ RU ]** Это библиотека для взаимодействия с 3-осевым акселерометром и гироскопом LSM6DS3.

## Functions

### SG_LSM6_Init(void)

**[ EN ]** Loads settings into LSM6 by SG_LSM6_SLAVE_ADDRESS *(can be configured in sg_config.h)*.

**[ RU ]** Загружает настройки в LSM6 с i2c адресом SG_LSM6_SLAVE_ADDRESS *(Можно настроить в sg_config.h)*.

### SG_LSM6_ReadData(uint8_t *data)

**[ EN ]** Accepts a pointer to an array[14] of uint8_t and writes data to the given array. See [datasheet (9.27-9.39)](https://content.arduino.cc/assets/st_imu_lsm6ds3_datasheet.pdf) for formatting info.

**[ RU ]** Принимает указатель на массив uint8 длинною в 14 и засаписывает в него данные. Формат указан в [тех. документации (9.27-9.39)](https://content.arduino.cc/assets/st_imu_lsm6ds3_datasheet.pdf).

----

Usage example:

```c
SG_I2C1_Init(); // this lib depends on SG's i2c lib

SG_LSM6_Init();

uint8_t lsm6_data[14];

SG_LIS3MDL_ReadData(lis3_data);

int16_t *ptr_temperature = (int16_t *)lsm6_data;
int16_t *ptr_gyr = (int16_t *)&lsm6_data[2];
int16_t *ptr_acc = (int16_t *)&lsm6_data[8];
// All following values are int16.
acc_x = ptr_acc[0]; acc_y = ptr_acc[1]; acc_z = ptr_acc[2];
gyr_x = ptr_gyr[0]; gyr_y = ptr_gyr[1]; gyr_z = ptr_gyr[2];
tmp = ptr_temperature[0]

```

----
Config example:

See [datasheet (9.13 and 9.14)](https://content.arduino.cc/assets/st_imu_lsm6ds3_datasheet.pdf) for more info.

```h
#ifndef __SG_CONFIG_H
#define __SG_CONFIG_H

//52Hz, +-4g, 400Hz filter
#define SG_LSM6_CTRL1_VALUE                 (3<<4 | 2<<2 | 0) 
//52Hz, 500 dps
#define SG_LSM6_CTRL2_VALUE                 (3<<4 | 1<<2 | 0)

#endif
```

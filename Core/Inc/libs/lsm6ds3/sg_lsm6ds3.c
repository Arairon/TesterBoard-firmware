#include "sg_lsm6ds3.h"

/* Чтение ID lsm6 - начало */
//HAL_StatusTypeDef spiStatus, spiStatus2;
/* Чтение ID lsm6 - конец*/

inline uint8_t SG_LSM6_ReadID() {
	return SG_LSM6_ReadRegister(0x0F);//who am i register
}

SG_LSM6_Status SG_LSM6_Init() {
	// delay for correct device start
	HAL_Delay(50);

	if(SG_LSM6_ReadID() != 0x6A) return SG_LSM6_DEVICE_ERROR;

	SG_LSM6_WriteTo(SG_LSM6_REG_CTRL1, SG_LSM6_CTRL1_VALUE);
	HAL_Delay(1);
	SG_LSM6_WriteTo(SG_LSM6_REG_CTRL2, SG_LSM6_CTRL2_VALUE);
	HAL_Delay(1);
	return SG_LSM6_DEVICE_OK;
}

/* Soft device reset */
void SG_LSM6_SoftReset() {
	// read register current data
	uint8_t ctrl3_cData = SG_LSM6_ReadRegister(0x12);
	// 1 - go to soft reset, after that, bit is automatically cleared
	SG_LSM6_WriteTo(0x12, ctrl3_cData | (1 << 0));
}

inline void SG_LSM6_ReadData(uint8_t* data) {
	SG_I2C_Read_Register_Multiple(SG_LSM6_SLAVE_ADDRESS, SG_LSM6_REG_DATA, data, 14);
}

inline uint8_t SG_LSM6_ReadRegister(uint8_t address) {
	return SG_I2C_Read_Register(SG_LSM6_SLAVE_ADDRESS, address);
}

inline void SG_LSM6_WriteTo(uint8_t address, uint8_t data) {
	SG_I2C_Write_Register(SG_LSM6_SLAVE_ADDRESS, address, data);
}

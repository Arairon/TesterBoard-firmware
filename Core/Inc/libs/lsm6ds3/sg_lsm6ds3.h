#ifndef SG_LSM6DS3_H
#define SG_LSM6DS3_H

#include "main.h"

#ifndef __SG_CONFIG_H
#warning It is recommended to create sg_config.h and include it before including this library
#endif

#define SG_LSM6_SLAVE_ADDRESS			0xD5

#define SG_LSM6_REG_DATA				0x20
#define SG_LSM6_REG_CTRL1				0x10
#define SG_LSM6_REG_CTRL2				0x11

#ifndef SG_LSM6_CTRL1_VALUE
#define SG_LSM6_CTRL1_VALUE						(3<<4 | 2<<2 | 0) //52Hz, +-4g, 400Hz filter
#warning SG_LSM6_CTRL1_VALUE defined by default:
#endif

#ifndef SG_LSM6_CTRL2_VALUE
#define SG_LSM6_CTRL2_VALUE						(3<<4 | 1<<2 | 0) //52Hz, 500 dps
#warning SG_LSM6_CTRL2_VALUE defined by default:
#endif

struct SG_LSM6_sensorVector {
	int16_t x, y, z;
};

typedef enum SG_LSM6_Status {
	SG_LSM6_DEVICE_ERROR,
	SG_LSM6_DEVICE_OK
} SG_LSM6_Status;

uint8_t SG_LSM6_ReadID();
SG_LSM6_Status SG_LSM6_Init();
void SG_LSM6_SoftReset();
void SG_LSM6_SoftMagnetometerCorrection();

void SG_LSM6_ReadData(uint8_t*);

uint8_t SG_LSM6_ReadRegister(uint8_t);
void SG_LSM6_WriteTo(uint8_t, uint8_t);

#endif

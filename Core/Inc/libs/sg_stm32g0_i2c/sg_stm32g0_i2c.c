#include <stm32g031xx.h>
#include "main.h"

extern I2C_HandleTypeDef hi2c2;

//tries to read byte from every possible address. Returns the number of slaves present on bus
uint8_t SG_I2C_Scan(){
	uint8_t test_data = 0, slave_count = 0;
	for (uint8_t i=0; i<128; i++){
		if (!HAL_I2C_Master_Receive(&hi2c2, i<<1, &test_data, 1, 100)) slave_count++;
	}
	return slave_count;
}
inline uint8_t SG_I2C_Write_Byte(uint8_t slave_addr, uint8_t data){
	return HAL_I2C_Master_Transmit(&hi2c2, slave_addr, &data, 1, SG_I2C_TIMEOUT);
}
inline uint8_t SG_I2C_Write_Register(uint8_t slave_addr, uint8_t reg_addr, uint8_t data){
	uint8_t tmp[]={reg_addr, data};
	return HAL_I2C_Master_Transmit(&hi2c2, slave_addr, tmp, 2, SG_I2C_TIMEOUT);
}
inline uint8_t SG_I2C_Read_Register(uint8_t slave_addr, uint8_t reg_addr){
	uint8_t tmp[]={reg_addr};
	HAL_I2C_Master_Transmit(&hi2c2, slave_addr, tmp, 1, SG_I2C_TIMEOUT);
	HAL_I2C_Master_Receive(&hi2c2, slave_addr, tmp, 1, SG_I2C_TIMEOUT);
	return tmp[0];
}
inline uint8_t SG_I2C_Read_Register_Multiple(uint8_t slave_addr, uint8_t reg_addr, uint8_t* data_out, uint8_t len){
	uint8_t tmp[]={reg_addr};
	HAL_I2C_Master_Transmit(&hi2c2, slave_addr, tmp, 1, SG_I2C_TIMEOUT);
	return HAL_I2C_Master_Receive(&hi2c2, slave_addr, data_out, len, SG_I2C_TIMEOUT);
}
/*uint8_t SG_I2C_Read_Register_MultipleRepeatStart(uint8_t slave_addr, uint8_t reg_addr, uint8_t* data_out, uint8_t len){

}*/

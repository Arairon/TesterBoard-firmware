#ifndef INC_SG_I2C_H_
#define INC_SG_I2C_H_

#include <stm32g031xx.h>

#define SG_I2C_TIMEOUT		10	//ms

//void SG_I2C1_Init(void);
uint8_t SG_I2C_Scan();
uint8_t SG_I2C_Write_Byte(uint8_t slave_addr, uint8_t data);
uint8_t SG_I2C_Write_Register(uint8_t slave_addr, uint8_t reg_addr, uint8_t data);
uint8_t SG_I2C_Read_Register(uint8_t slave_addr, uint8_t reg_addr);
uint8_t SG_I2C_Read_Register_Multiple(uint8_t slave_addr, uint8_t reg_addr, uint8_t* data_out, uint8_t len);
uint8_t SG_I2C_Read_Register_MultipleRepeatStart(uint8_t slave_addr, uint8_t reg_addr, uint8_t* data_out, uint8_t len);

#endif /* INC_SG_I2C_H_ */

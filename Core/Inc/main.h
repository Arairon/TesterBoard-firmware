/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdint.h>
#include <stdio.h>
#include <stm32g031xx.h>
#include <string.h>
#include "sg_config.h"
#include "libs/sg_stm32g0_i2c/sg_stm32g0_i2c.h"
#include "libs/lis3mdl/SG_LIS3MDL.h"
#include "libs/lsm6ds3/sg_lsm6ds3.h"
#include "libs/bmp280/sg_bmp280.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define TesterC_Pin GPIO_PIN_14
#define TesterC_GPIO_Port GPIOC
#define TesterB_Pin GPIO_PIN_15
#define TesterB_GPIO_Port GPIOC
#define TesterA_Pin GPIO_PIN_0
#define TesterA_GPIO_Port GPIOA
#define TesterRootA_Pin GPIO_PIN_4
#define TesterRootA_GPIO_Port GPIOA
#define TesterRootB_Pin GPIO_PIN_5
#define TesterRootB_GPIO_Port GPIOA
#define TesterRootC_Pin GPIO_PIN_0
#define TesterRootC_GPIO_Port GPIOB
#define TesterInput_Pin GPIO_PIN_1
#define TesterInput_GPIO_Port GPIOB
#define BranchB_Pin GPIO_PIN_8
#define BranchB_GPIO_Port GPIOA
#define BranchA_Pin GPIO_PIN_6
#define BranchA_GPIO_Port GPIOC
#define BranchC_Pin GPIO_PIN_15
#define BranchC_GPIO_Port GPIOA
#define RootC_Pin GPIO_PIN_3
#define RootC_GPIO_Port GPIOB
#define RootB_Pin GPIO_PIN_4
#define RootB_GPIO_Port GPIOB
#define RootA_Pin GPIO_PIN_5
#define RootA_GPIO_Port GPIOB
#define RootInhibit_Pin GPIO_PIN_8
#define RootInhibit_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

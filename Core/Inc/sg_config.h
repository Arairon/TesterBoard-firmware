#ifndef __SG_CONFIG_H
#define __SG_CONFIG_H


#define SG_BMP280_SLAVE_ADDRESS			0b11101101

#define SG_LSM6_CTRL1_VALUE						(3<<4 | 2<<2 | 0) //52Hz, +-4g, 400Hz filter
#define SG_LSM6_CTRL2_VALUE						(3<<4 | 1<<2 | 0) //52Hz, 500 dps

#define	SG_LIS3MDL_SLAVE_ADDRESS (28<<1)  // Needs verification. Altium variant expects 28. Lib expects 30.
#define LIS3MDL_REG1_VALUE					0b01010000 // LIS3MDL_REG1_VALUE defined by default: TEMP_EN - OFF, XY mode - High performance mode, Data rate - 10Hz, Self-test - disabled
#define LIS3MDL_REG2_VALUE					0b00000000 // LIS3MDL_REG2_VALUE defined bt default: Full-scale - +-4 gauss, REBOOT - normal mode, SOFT_RST - default

#endif // !__SG_CONFIG_H
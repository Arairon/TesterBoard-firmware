/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c2;

SPI_HandleTypeDef hspi1;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C2_Init(void);
static void MX_SPI1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */
uint8_t readPin(void);
void send_err(uint8_t, uint8_t);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

#define PIN_READ_DELAY 10

uint8_t croot = 0;
uint8_t cbr = 0;
uint8_t ctroot = 0;
uint8_t ctbr = 0;
uint8_t cmd[32];
uint8_t cmd_ready = 0;
uint8_t pointer = 0;
uint8_t report[8];
uint8_t chkmap[8];

uint8_t testerInhibit = 0;

struct sensor_data_struct {
  // bmp280 6b
  int16_t temp;
  uint32_t pressure;
  // lsm6 14b
  uint16_t acc[3];
  uint16_t gyr[3];
  int16_t temp_lsm;
  // lis3 6b
  uint16_t mag[3];
}; // 26b

struct sensor_data_struct sensor_report;

uint8_t int_init = 0;
uint8_t bmp_init = 0;
uint8_t lsm_init = 0;
uint8_t lis_init = 0;
uint8_t lsm6_data[14];
uint8_t lis3_data[6];
uint8_t test_data = 0;

void rootCh(uint8_t ch) {
  if (ch > 7) {
    send_err(1, ch);
    return;
  }

  HAL_GPIO_WritePin(RootA_GPIO_Port, RootA_Pin, ch & 0b001);
  HAL_GPIO_WritePin(RootB_GPIO_Port, RootB_Pin, ch & 0b010);
  HAL_GPIO_WritePin(RootC_GPIO_Port, RootC_Pin, ch & 0b100);
  croot = (ch >= 4 ? ch - 4 : ch + 4);
}

void branchCh(uint8_t ch) {
  if (ch > 7) {
    send_err(1, ch);
    return;
  }

  HAL_GPIO_WritePin(BranchA_GPIO_Port, BranchA_Pin, ch & 0b001);
  HAL_GPIO_WritePin(BranchB_GPIO_Port, BranchB_Pin, ch & 0b010);
  HAL_GPIO_WritePin(BranchC_GPIO_Port, BranchC_Pin, ch & 0b100);
  cbr = ch;
}

void testerCh(uint8_t ch) {
  if (ch > 7) {
    send_err(1, ch);
    return;
  }

  HAL_GPIO_WritePin(TesterA_GPIO_Port, TesterA_Pin, ch & 0b001);
  HAL_GPIO_WritePin(TesterB_GPIO_Port, TesterB_Pin, ch & 0b010);
  HAL_GPIO_WritePin(TesterC_GPIO_Port, TesterC_Pin, ch & 0b100);
  ctbr = ch;
}

void testerRootCh(uint8_t ch) {
  if (ch > 7) {
    send_err(1, ch);
    return;
  }

  HAL_GPIO_WritePin(TesterRootA_GPIO_Port, TesterRootA_Pin, ch & 0b001);
  HAL_GPIO_WritePin(TesterRootB_GPIO_Port, TesterRootB_Pin, ch & 0b010);
  HAL_GPIO_WritePin(TesterRootC_GPIO_Port, TesterRootC_Pin, ch & 0b100);
  ctroot = ch;
}

void tsel(uint8_t ch) {
  if (ch > 64 || ch < 1) {
    send_err(1, ch);
    return;
  }
  ch--;
  uint8_t tch = ch / 8;
  rootCh(tch >= 4 ? tch - 4 : tch + 4);
  branchCh(ch % 8);
}

void bsel(uint8_t ch) {
  if (ch > 64 || ch < 1) {
    send_err(1, ch);
    return;
  }
  ch--;
  testerRootCh(ch / 8);
  testerCh(ch % 8);
}

void readMap(uint8_t reverse) {
  memset(report, 0, 8);
  if (testerInhibit) {
    send_err(0, 'i');
    return;
  }
  for (uint8_t p = 0; p < 8; p++) {
    uint8_t page = 0;
    for (uint8_t i = 0; i < 8; i++) {
      if (reverse) {
        tsel(p * 8 + i + 1);
      } else {
        bsel(p * 8 + i + 1);
      }
      page |= (readPin()) << i;
    }
    report[p] = page;
  }
}

char msg[50];
void clrmsg() { memset(msg, 0, 50); }

void send() {
  HAL_UART_Transmit(&huart2, msg, strlen(msg), strlen(msg) * 2);
  clrmsg();
}

void sendn(uint8_t n) {
  HAL_UART_Transmit(&huart2, msg, n, n * 2);
  clrmsg();
}

void send_err(uint8_t code, uint8_t extra) {
  snprintf(msg, 50, "ERR>%i>%i;", code, extra);
  send();
  HAL_Delay(100);
}

void read_sensors() {
  if (!int_init) {
    snprintf(msg, 50, "ERR>0>Not initialized;", cmd[0]);
    send();
    return;
  }
  memset((uint8_t *)&sensor_report, 0, sizeof(sensor_report));
  int16_t *ptr_g = (int16_t *)&lsm6_data[2];
  int16_t *ptr_a = (int16_t *)&lsm6_data[8];
  int16_t *ptr_m = (int16_t *)lis3_data;
  if (bmp_init) {
    SG_BMP280_read_t_and_p(&sensor_report.pressure, &sensor_report.temp);
  }
  if (lsm_init) {
    SG_LSM6_ReadData(lsm6_data);
  }
  if (lis_init) {
    SG_LIS3MDL_ReadData(lis3_data);
  }
  for (uint8_t i = 0; i < 3; i++) {
    sensor_report.acc[i] = ptr_a[i];
    sensor_report.gyr[i] = ptr_g[i];
    sensor_report.mag[i] = ptr_m[i];
  }
  sensor_report.temp_lsm = lsm6_data[0] << 8 | lsm6_data[1];
}

void disable_inhibit() {
  testerInhibit = 0;
  HAL_GPIO_WritePin(RootInhibit_GPIO_Port, RootInhibit_Pin, GPIO_PIN_RESET);
}
void enable_inhibit() {
  testerInhibit = 1;
  HAL_GPIO_WritePin(RootInhibit_GPIO_Port, RootInhibit_Pin, GPIO_PIN_SET);
}

uint8_t readPin() {
  if (testerInhibit) {
    send_err(0, 'i');
    return 2;
  }
  HAL_Delay(PIN_READ_DELAY); // Too fast causes false positives.
  return HAL_GPIO_ReadPin(TesterInput_GPIO_Port, TesterInput_Pin);
}

void sendStatus() {
  snprintf(msg, 50, "ST>%i:%i>%i:%>%i:%i%i%i;", croot, cbr, ctroot, ctbr,
           int_init, bmp_init, lsm_init, lis_init);
  send();
}

void ack() {
  snprintf(msg, 50, "ACK;");
  send();
  HAL_Delay(10);
}

void ok() {
  HAL_Delay(10);
  snprintf(msg, 50, "OK;");
  send();
}
void fin() {
  HAL_Delay(10);
  snprintf(msg, 50, "FIN;");
  send();
}

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick.
   */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
#ifdef HANDLED_BY_USERCODE
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C2_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

#endif
  MX_GPIO_Init();        // duh
  MX_USART2_UART_Init(); // Main communication interface
  tsel(1);
  bsel(1);
  uint8_t t = 0;

  USART2->CR1 |= USART_CR1_RXNEIE_RXFNEIE;

  snprintf(msg, 50, "START;");
  send();
  HAL_Delay(10);
  while (1) {
    HAL_UART_Receive(&huart2, cmd, 32, 0xff);
    if (cmd[0] == 'A') {
      ok();
      break;
    } else if (cmd[0] != 0) {
      snprintf(msg, 50, "START;");
      send();
    }
    HAL_Delay(10);
    memset(cmd, 0, 32);
  }
  memset(cmd, 0, 32);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1) {
    // HAL_Delay(1);

    while (!cmd_ready) {
      USART2->ICR |= USART_ICR_ORECF;
      USART2->CR1 &= ~USART_CR1_RXNEIE_RXFNEIE;
      HAL_UART_Receive(&huart2, cmd, 32, 0xffff);
      cmd_ready = cmd[31];
      HAL_Delay(10);
      USART2->CR1 |= USART_CR1_RXNEIE_RXFNEIE;
    }

    ack();
    if (int_init && (cmd[0] > 0 && cmd[0] < 100)) {
      snprintf(msg, 50, "ERR>0>%i;", cmd[0]);
      send();
      cmd[0] = 200;
    }
    if (!int_init && (cmd[0] > 100 && cmd[0] < 200)) {
      snprintf(msg, 50, "ERR>0>%i;", cmd[0]);
      send();
      cmd[0] = 200;
    }

    switch (cmd[0]) {
    case 0:
      snprintf(msg, 50, "RESET;");
      send();
      HAL_Delay(100);
      HAL_NVIC_SystemReset();
      break;
    case 2: // read currently selected pins against each other
      snprintf(msg, 50, "PIN>%i:%i>%i:%i>%i;", croot, cbr, ctroot, ctbr,
               readPin());
      send();
      break;
    case 10: // get status
      sendStatus();
      break;
    case 11: // read pin1 against pin2
      tsel(cmd[1]);
      bsel(cmd[2]);
      snprintf(msg, 50, "PIN>%i:%i>%i:%i>%i;", croot, cbr, ctroot, ctbr,
               readPin());
      send();
      break;
    case 12: // read pin1 against all, returns map
      tsel(cmd[1]);
      clrmsg();
      snprintf(msg, 50, "MAP>12>%i:%i>", croot, cbr);
      readMap(0);
      t = strlen(msg);
      memcpy(msg + strlen(msg), report, 8);
      msg[t + 8] = ';';
      msg[t + 9] = '\0';
      sendn(t + 9);

      break;
    case 13: // all pins against pin1, returns map
      bsel(cmd[1]);
      clrmsg();
      snprintf(msg, 50, "MAP>13>%i:%i>", ctroot, ctbr);
      readMap(1);
      t = strlen(msg);
      memcpy(msg + strlen(msg), report, 8);
      msg[t + 8] = ';';
      msg[t + 9] = '\0';
      sendn(t + 9);
      break;
    case 14: // all against all, returns 64 maps
      for (uint8_t i = 1; i < 65; i++) {
        tsel(i);
        clrmsg();
        snprintf(msg, 50, "PMAP>%i:%i>", croot, cbr);
        readMap(0);
        t = strlen(msg);
        memcpy(msg + strlen(msg), report, 8);
        msg[t + 8] = ';';
        msg[t + 9] = '\0';
        sendn(t + 9);
        HAL_Delay(20);
      }
      fin();
      break;
    case 15: // map against map, returnx 64 maps; map1: cmd[1]:cmd[8],
             // map2: cmd[9]:cmd[16]
      if (testerInhibit) {
        send_err(0, 'i');
        break;
      }
      for (uint8_t pin = 0; pin < 64; pin++) {
        memset(report, 0, 8);
        if ((cmd[1 + (pin / 8)] & (0b1 << (pin % 8)))) { // if in map1
          tsel(pin + 1);
          for (uint8_t p = 0; p < 8; p++) {
            uint8_t page = 0;
            for (uint8_t i = 0; i < 8; i++) {
              if (!(cmd[9 + p] & (0b1 << i)))
                continue; // if not in map2
              bsel(p * 8 + i + 1);
              page |= (readPin()) << i;
            }
            report[p] = page;
          }
        }
        clrmsg();
        snprintf(msg, 50, "PMAP>%i:%i>", croot, cbr);
        t = strlen(msg);
        memcpy(msg + strlen(msg), report, 8);
        msg[t + 8] = ';';
        msg[t + 9] = '\0';
        sendn(t + 9);
        HAL_Delay(20);
      }
      break;
    case 16:
      if (testerInhibit) {
        send_err(0, 'i');
        break;
      }
      memset(report, 0, 8);
      for (uint8_t i = 0; i < 64; i++) {
        tsel(i + 1);
        bsel(i + 1);
        report[i / 8] |= (readPin()) << (i % 8);
      }
      clrmsg();
      snprintf(msg, 50, "TMAP>");
      t = strlen(msg);
      memcpy(msg + strlen(msg), report, 8);
      msg[t + 8] = ';';
      msg[t + 9] = '\0';
      sendn(t + 9);

    case 19: // select pins
      if (cmd[1]) {
        tsel(cmd[1]);
      }
      if (cmd[2]) {
        bsel(cmd[2]);
      }
      break;

    case 65:
      snprintf(msg, 50, "RESET;");
      send();
      HAL_Delay(100);
      HAL_NVIC_SystemReset();
      break;

    case 100: // init interfaces
      if (!int_init) {
        tsel(33);
        bsel(1);
        enable_inhibit();
        HAL_Delay(100);
        MX_I2C2_Init();
        MX_SPI1_Init();
        MX_USART1_UART_Init();
        int_init = 1;
      } else {
        snprintf(msg, 50, "ERR>0>%i;", cmd[0]);
        send();
      }
      break;

    case 101: // Scan
      snprintf(msg, 50, "SCAN>");
      t = strlen(msg);
      for (uint8_t i = 0; i < 128; i++) {
        if (!HAL_I2C_Master_Receive(&hi2c2, i << 1, &test_data, 1, 100)) {
          msg[t++] = i;
        }
      }
      msg[t++] = ';';
      send();
      break;
    case 105: // init sensor[1] or all
      switch (cmd[1]) {
      case 0:
        bmp_init = 1;
        lsm_init = 1;
        lis_init = 1;
        SG_BMP280_init();
        SG_LSM6_Init();
        SG_LIS3MDL_init();
        break;
      case 1:
        bmp_init = 1;
        SG_BMP280_init();
        break;
      case 2:
        lsm_init = 1;
        SG_LSM6_Init();
        break;
      case 3:
        lis_init = 1;
        SG_LIS3MDL_init();
        break;
      }

      break;

    case 110: // read sensor_report
      read_sensors();
      snprintf(msg, 50, "SENS>");
      t = strlen(msg);
      msg[t++] = lis_init << 2 | lsm_init << 1 | bmp_init;
      msg[t++] = '>';
      uint8_t *const ptr = (uint8_t *)&sensor_report;
      for (uint8_t i = 0; i < sizeof(sensor_report); i++) {
        msg[t++] = ptr[i];
      }
      msg[t++] = ';';
      sendn(t);
      break;

    case 200:
      break; // no op

    case 250: // test
      msg[324678] = 1;
      break;
    case 255: // set inhibit
      if (cmd[1] == 1 || cmd[1] == 0) {
        if (cmd[1] == testerInhibit) {
          snprintf(msg, 50, "ERR>2>Inhibit already set to %i;", cmd[1]);
          send();
        } else {
          if (cmd[1]) {
            enable_inhibit();
          } else {
            disable_inhibit();
          }
        }

      } else {
        snprintf(msg, 50, "ERR>1>Inhib: %i;", cmd[1]);
        send();
      }
      break;

    default:
      snprintf(msg, 50, "ERR>3>%i;", cmd[0]);
      send();
    }
    memset(cmd, 0, 32);
    cmd_ready = 0;
    HAL_Delay(100);
    ok();

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
   */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
   * in the RCC_OscInitTypeDef structure.
   */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
   */
  RCC_ClkInitStruct.ClockType =
      RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK) {
    Error_Handler();
  }
}

/**
 * @brief I2C2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_I2C2_Init(void) {

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.Timing = 0x00303D5B;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK) {
    Error_Handler();
  }

  /** Configure Analogue filter
   */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK) {
    Error_Handler();
  }

  /** Configure Digital filter
   */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK) {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */
}

/**
 * @brief SPI1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_SPI1_Init(void) {

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK) {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */
}

/**
 * @brief USART1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART1_UART_Init(void) {

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK) {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart1, UART_TXFIFO_THRESHOLD_1_8) !=
      HAL_OK) {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart1, UART_RXFIFO_THRESHOLD_1_8) !=
      HAL_OK) {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart1) != HAL_OK) {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */
}

/**
 * @brief USART2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART2_UART_Init(void) {

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK) {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */
}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void) {
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  /* USER CODE BEGIN MX_GPIO_Init_1 */
  /* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, TesterC_Pin | TesterB_Pin | BranchA_Pin,
                    GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA,
                    TesterA_Pin | TesterRootA_Pin | TesterRootB_Pin |
                        BranchB_Pin | BranchC_Pin,
                    GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB,
                    TesterRootC_Pin | RootC_Pin | RootB_Pin | RootA_Pin |
                        RootInhibit_Pin,
                    GPIO_PIN_RESET);

  /*Configure GPIO pins : TesterC_Pin TesterB_Pin BranchA_Pin */
  GPIO_InitStruct.Pin = TesterC_Pin | TesterB_Pin | BranchA_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : TesterA_Pin TesterRootA_Pin TesterRootB_Pin
     BranchB_Pin BranchC_Pin */
  GPIO_InitStruct.Pin = TesterA_Pin | TesterRootA_Pin | TesterRootB_Pin |
                        BranchB_Pin | BranchC_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : TesterRootC_Pin RootC_Pin RootB_Pin RootA_Pin
                           RootInhibit_Pin */
  GPIO_InitStruct.Pin =
      TesterRootC_Pin | RootC_Pin | RootB_Pin | RootA_Pin | RootInhibit_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : TesterInput_Pin */
  GPIO_InitStruct.Pin = TesterInput_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(TesterInput_GPIO_Port, &GPIO_InitStruct);

  /* USER CODE BEGIN MX_GPIO_Init_2 */
  /* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1) {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line) {
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line
                                   number, ex: printf("Wrong parameters value:
     file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

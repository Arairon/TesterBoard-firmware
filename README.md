# Firmware for Arai's TesterBoard v0.1

## Connection

TesterBoard uses an onboard usb-uart converter with baudrate set to 115200.

When started, TB sends a "START;" message and expects a single 'A' in return.

TB then accepts commands with a set length of 32. 32nd byte must always be set to something other than 0, otherwise the command will be ignored.

When TB acknowledges a command it sends an "ACK;" in response. Once the command is completed it sends an "OK;"

[//]: <> (## Comments)
[//]: <> (yep, empty. :\))

## List of commands

A command is the first byte.  
All following bytes are considered arguments.  
Passing more arguments than expected will not raise an error.  
Passing less arguments than expected will be equivalent to passing 0 as each argument.

- 0 - Reset
- RRR - Force reset

- 2 - Tests currently selected pins against each other, returns PIN>{pos}>value;

- ... - unmapped

- 10 - Get current status
- 11[2] - Selects pins [1] and [2], tests them against each other and returns PIN>{pos}{value};
- 12[1] - Tests pin [1] against all, returns MAP>12>{pos1}>{map_value}
- 13[1] - Tests all pins against pin [1], returns MAP>13>{pos2}>{map_value}
- 14 - Tests all against all, returns PMAP>{pos1}>{map_value}; x64
- 15 - Tests pins in map [1] against pins in map[2], returns PMAP>{pos1}>{map_value}; x64
- 16 - Vertical map. Test pin 1 against pin 1, pin 2 against pin 2, and so on. returns TMAP>{map_value};

- ... - unmapped

- 19[2] - Select pins [1] and [2]

- ... - unmapped

- 65 - This value is used for starting handshake and will raise an error used as a command.

- ... - unmapped

- 100 - Enable interfaces // Not all interfaces are production ready.
- 101 - Try reading a single byte from each i2c address. Appends the address if successful. Returns SCAN>{addresses (or nothing)};
- 105[1] - Init sensor [1] or init all sensors if [1] is 0
- 110 - Read sensor report. Returns SENS>{see actual code}

Some explanation:

- [N] either means amount of accepted arguments. Or the actual passed argument at position N
- {pos1} is croot:cbr
- {pos2} is ctroot:ctbr
- {pos} is croot:cbr>ctroot:ctbr
- {value} is either 1 or 0
- {map_value} is 8 bytes, where each bit represents a pin
